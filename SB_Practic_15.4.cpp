﻿#include <iostream>
#include <locale.h>

void FindOddNumbers(int N, bool IsOdd)
{
    for (int i = 0; i <= N; i++)
    {
        if ((i % 2 == 0) == IsOdd)
        {
            std::cout << i << " ";
        }
    }

    std::cout << std::endl;
}

int main()
{
    setlocale(LC_ALL, "Russian");

    const int N = 10;

    std::cout << "Четные" << ": ";
    FindOddNumbers(N, true);

    std::cout << "Нечетные" << ": ";
    FindOddNumbers(N, false);

    return 0;
}

